//----------------------------------------------------------------------------

#ifndef IMG_PIXEL_HPP
#define IMG_PIXEL_HPP 1

#include <cstdint>

namespace img {

struct Pixel 
{
  std::uint8_t r{}, g{}, b{};
};


// Pixel <-- Pixel * double
inline Pixel operator*(const Pixel &p, double k) {
  return {
    (std::uint8_t) (p.r*k),
    (std::uint8_t) (p.g*k),
    (std::uint8_t) (p.b*k)
  };
}

// Pixel <-- Pixel + Pixel
inline Pixel operator+(const Pixel &p1, const Pixel &p2) {
  return {  
    (std::uint8_t)(p1.r + p2.r),
    (std::uint8_t)(p1.g + p2.g),
    (std::uint8_t)(p1.b + p2.b) 
  };
}

// Pixel <-- Pixel << int
inline Pixel operator<<(const Pixel &p, int k) {
  return {
    (std::uint8_t) (p.r << k),
    (std::uint8_t) (p.g << k),
    (std::uint8_t) (p.b << k)
  };
}

// Pixel <-- Pixel >> int
inline Pixel operator>>(const Pixel &p, int k) {
  return {
    (std::uint8_t) (p.r >> k),
    (std::uint8_t) (p.g >> k),
    (std::uint8_t) (p.b >> k)
  };
}

// Pixel <-- Pixel & int
inline Pixel operator&(const Pixel &p, int k) {
  return {
    (std::uint8_t) (p.r & k),
    (std::uint8_t) (p.g & k),
    (std::uint8_t) (p.b & k)
  };
}

// Pixel <-- Pixel | Pixel
inline Pixel operator|(const Pixel &p1, const Pixel &p2) {
  return {
    (std::uint8_t) (p1.r | p2.r),
    (std::uint8_t) (p1.g | p2.g),
    (std::uint8_t) (p1.b | p2.b)
  };
}

// merge pixels from image1 with pixels from image2
// - image1 is read-only and has dimensions w1*h1
// - image2 is read-only and has dimensions w2*h2
// - resulting image has dimensions w1*h1
// using a generic merge operation on pixels
// - Pixel <-- oper(Pixel, Pixel)

template<typename LambdaFunction>
inline
void 
merge(int w1,
      int h1, 
      const Pixel *d1, 
      int w2,
      int h2,
      const Pixel *d2,
      Pixel *d, 
      LambdaFunction lf
    ) {
  
  double r_h = (double) h2/h1;
  double r_w = (double) w2/w1;

  for (int y1=0; y1 < h1; y1++) {
    int y2 = (int) (y1*r_h);
    for (int x1=0; x1 < w1; x1++) {
      int x2 = (int) (x1*r_w);
      int i1 = (x1 + y1 * w1);
      int i2 = (x2 + y2 * w2);
      d[i1] = lf(d1[i1], d2[i2]);
    }
  }
}

} // namespace img

#endif // IMG_PIXEL_HPP

//----------------------------------------------------------------------------