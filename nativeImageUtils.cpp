//----------------------------------------------------------------------------

#if defined _WIN32
# define NATIVE_IMAGE_UTILS_API extern "C" __declspec(dllexport)
#else
# define NATIVE_IMAGE_UTILS_API extern "C"
#endif

#include <cstdint>
#include <iostream>
#include <string>
#include <algorithm>
#include "pixel.hpp"

//----------------------------------------------------------------------------
// Minimal function called from Python
//----------------------------------------------------------------------------

NATIVE_IMAGE_UTILS_API
int
hello(const char *msg)
{
  std::string s{msg};
  int result=int(s.size());
  std::cout << "native hello() received '" << msg
            << "' and is about to return " << result << '\n';
  return result;
}

//----------------------------------------------------------------------------
// Blend images
//----------------------------------------------------------------------------

NATIVE_IMAGE_UTILS_API
void
imgBlend(int w1,               // first input image (and result) width
         int h1,               // first input image (and result) height
         const img::Pixel *d1, // first input image pixels (w1*h1)
         int w2,               // second input image width
         int h2,               // second input image height
         const img::Pixel *d2, // second input image pixels (w2*h2)
         int k,                // cursor value within range [0;100]
         img::Pixel *d)        // resulting image pixels (w1*h1)
{
  /*(void)w1; (void)h1; (void)d1; // avoid ``unused parameter'' warnings
  (void)w2; (void)h2; (void)d2;
  (void)k; (void)d;*/

  // ... À COMPLÉTER ...
  //
  // renseigner les `w1*h1' pixels de `d'
  //

#if 0 // uniform colour

  std::for_each(d, d+w1*h1, [](auto &element) {
    element = {80, 100, 200};
  });


#elif 0 // uniform colour dimmed according to k

std::for_each(d, d+w1*h1, [&k](auto &element) {
  img::Pixel p = {80, 100, 200};
  element = std::move(p*(k/100.0));
});

#elif 0 // copy of d1 dimmed according to k

std::transform(d1, d1+w1*h1, d, [&k](const auto &element) {
  return std::move(element * (k/100.0));
});

#elif 0 // full problem (hardcoded loops)

for (int y1=0; y1 < h1; y1++) {
  for (int x1=0; x1 < w1; x1++) {
    int x2 = (int) (x1*w2/w1);
    int y2 = (int) (y1*h2/h1);
    int i1 = (x1 + y1 * w1);
    int i2 = (x2 + y2 * w2);
    d[i1] = d1[i1]*((100-k)/100.0) + d2[i2]*(k/100.0);
  }
}

#else // full problem (generic loops)
  merge(w1, h1, d1, 
        w2, h2, d2, 
        d, 
        [&k](const auto p1, const auto p2) {
    return p1*((100-k)/100.0) + p2*(k/100.0);
});

#endif
}

//----------------------------------------------------------------------------
// Reveal an image hidden into another one
//----------------------------------------------------------------------------

NATIVE_IMAGE_UTILS_API
void
imgReveal(int w1,               // input image (and result) width
          int h1,               // input image (and result) height
          const img::Pixel *d1, // input image pixels (w1*h1)
          int k,                // cursor value within range [0;8]
          img::Pixel *d)        // resulting image pixels (w1*h1)
{
  /*(void)w1; (void)h1; (void)d1; // avoid ``unused parameter'' warnings
  (void)k; (void)d;*/

  // ... À COMPLÉTER ...
  //
  // renseigner les `w1*h1' pixels de `d'
  //

  // similar to copy of d1 dimmed according to k
  std::transform(d1, d1+w1*h1, d, [&k](const auto &element) {
    return std::move(element << (8-k));
  });
}

//----------------------------------------------------------------------------
// Hide an image into another one
//----------------------------------------------------------------------------

NATIVE_IMAGE_UTILS_API
void
imgHide(int w1,               // first input image (and result) width
        int h1,               // first input image (and result) height
        const img::Pixel *d1, // first input image pixels (w1*h1)
        int w2,               // second input image width
        int h2,               // second input image height
        const img::Pixel *d2, // second input image pixels (w2*h2)
        int k,                // cursor value within range [0;8]
        img::Pixel *d)        // resulting image pixels (w1*h1)
{
  /*(void)w1; (void)h1; (void)d1; // avoid ``unused parameter'' warnings
  (void)w2; (void)h2; (void)d2;
  (void)k; (void)d;*/

  // ... À COMPLÉTER ...
  //
  // renseigner les `w1*h1' pixels de `d'
  //

  // similar to blending
/*   for (int y1=0; y1 < h1; y1++) {
    for (int x1=0; x1 < w1; x1++) {
      int x2 = (int) (x1*w2/w1);
      int y2 = (int) (y1*h2/h1);
      int i1 = (x1 + y1 * w1);
      int i2 = (x2 + y2 * w2);
      d[i1] = (d1[i1]&k) | (d2[i2] >> (8-k));
    }
  } */

  merge(w1, h1, d1, 
        w2, h2, d2, 
        d, 
        [&k](const auto p1, const auto p2) {
    return (p1&(255<<k)) | (p2 >> (8-k));
});

}

//----------------------------------------------------------------------------